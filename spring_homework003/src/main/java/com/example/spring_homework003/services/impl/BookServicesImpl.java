package com.example.spring_homework003.services.impl;

;
import com.example.spring_homework003.entity.Book;
import com.example.spring_homework003.entity.request.BookRequest;
import com.example.spring_homework003.exception.EmptyDataException;
import com.example.spring_homework003.exception.NotFoundException;
import com.example.spring_homework003.repository.AuthorRepository;
import com.example.spring_homework003.repository.BookRepository;
import com.example.spring_homework003.repository.CategoryRepository;
import com.example.spring_homework003.services.AuthorServices;
import com.example.spring_homework003.services.BookServices;
import com.example.spring_homework003.services.CategoryServices;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
public class BookServicesImpl implements BookServices {


    private final BookRepository repository;


    private final CategoryServices categoryServices;


    private final AuthorServices authorServices;

    //get by id
    @Override
    public Book getById(Integer id) {

        if (repository.getById(id) == null) {
            throw new NotFoundException("Book ID : " + id + " Don't have in Database  😭🥲!!!");
        }

        return repository.getById(id);
    }

    //get all
    @Override
    public List<Book> gets() {

        if (repository.gets().isEmpty()) {
            throw new NotFoundException("Empty Data in Your Database 🥲😭!!!");
        }

        return repository.gets();
    }

    //insert
    @Override
    public Integer insert(BookRequest book) {


        authorServices.getById(book.getAuthorId());

        if (book.getTitle().isEmpty()) {
            throw new EmptyDataException("Your Title must be have value 🥲😭 !!!");
        } else if (book.getTitle().length() <= 4) {
            throw new EmptyDataException("Your Title must be have 4 char up 🥲😭 !!!");
        }
        Integer id = repository.insertBook(book);
        for (int i = 0; i < book.getCategory().size(); i++) {

            categoryServices.getById(book.getCategory().get(i));
            repository.insertBookDetail(id, book.getCategory().get(i));
        }
        return id;

    }

    //delete
    @Override
    public void delete(Integer id) {

        Book book = getById(id);

        repository.delete(book.getId());
    }

    //update
    @Override
    public Integer update(Integer id, BookRequest book) {

        authorServices.getById(book.getAuthorId());

        if (book.getTitle().isEmpty()) {
            throw new EmptyDataException("Your Title must be have value 🥲😭 !!!");
        } else if (book.getTitle().length() <= 4) {
            throw new EmptyDataException("Your Title must be have 4 char up 🥲😭 !!!");
        }

        Integer bookId = repository.update(id, book);
        repository.deleteBookDetails(bookId);
        for (int i = 0; i < book.getCategory().size(); i++) {
            categoryServices.getById(book.getCategory().get(i));
            repository.insertBookDetail(bookId, book.getCategory().get(i));
        }

        return bookId;
    }

}
