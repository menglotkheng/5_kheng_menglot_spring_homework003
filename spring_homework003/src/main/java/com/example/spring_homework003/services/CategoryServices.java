package com.example.spring_homework003.services;

import com.example.spring_homework003.dto.AuthorDTO;
import com.example.spring_homework003.dto.CategoryDTO;
import com.example.spring_homework003.entity.request.AuthorRequest;
import com.example.spring_homework003.entity.request.CategoryRequest;

import java.util.List;

public interface CategoryServices {

    CategoryDTO insert(CategoryRequest category);

    CategoryDTO getById(Integer id);

    List<CategoryDTO> gets();

    CategoryDTO update(Integer id, CategoryRequest category);

    void delete(Integer id);

}
