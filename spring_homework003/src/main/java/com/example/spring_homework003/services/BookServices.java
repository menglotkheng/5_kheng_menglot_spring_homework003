package com.example.spring_homework003.services;

import com.example.spring_homework003.dto.BookDTO;

import com.example.spring_homework003.entity.Book;
import com.example.spring_homework003.entity.request.BookRequest;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface BookServices {


    //get by id
    Book getById(Integer id);

    //get all
    List<Book> gets();

    //insert
     Integer insert(BookRequest book);

     //delete
    void delete(Integer id);

    //update
    Integer update(Integer id,BookRequest book);

}
