package com.example.spring_homework003.services.impl;

import com.example.spring_homework003.dto.AuthorDTO;
import com.example.spring_homework003.entity.request.AuthorRequest;
import com.example.spring_homework003.exception.EmptyDataException;
import com.example.spring_homework003.exception.NotFoundException;
import com.example.spring_homework003.mapper.AuthorMapper;
import com.example.spring_homework003.repository.AuthorRepository;
import com.example.spring_homework003.services.AuthorServices;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class AuthorServicesImpl implements AuthorServices {

    private final AuthorRepository repository;


    //insert
    @Override
    public AuthorDTO insert(AuthorRequest author) {


        if (author.getName().isEmpty() || author.getGender().isEmpty()) {

            throw new EmptyDataException("Required Data!!!");

        } else if (author.getName().length() <= 4) {
            throw new EmptyDataException("Required Your Data must have 4 char up !!!");
        } else if (!(author.getGender().toLowerCase().equals("male") || author.getGender().toLowerCase().equals("female"))) {

            throw new EmptyDataException("Required Gender Must be : Male or Female !!!");

        }

        return AuthorMapper.INSTANCE.toDto(repository.insert(author));
    }

    //get by id
    @Override
    public AuthorDTO getById(Integer id) {

        if (repository.getById(id) == null) {
            throw new NotFoundException("Author ID : " + id + " Don't have in Database  😭🥲!!!");
        }

        return AuthorMapper.INSTANCE.toDto(repository.getById(id));
    }

    //get all
    @Override
    public List<AuthorDTO> gets() {

        if (repository.gets().isEmpty()) {
            throw new NotFoundException("Empty Data in Your Database 🥲😭!!!");
        }

        return repository.gets().stream().map(AuthorMapper.INSTANCE::toDto).toList();
    }


    //update
    @Override
    public AuthorDTO update(Integer id, AuthorRequest author) {

        getById(id);

        if (author.getName().isEmpty() || author.getGender().isEmpty()) {

            throw new EmptyDataException("Required Data!!!");

        } else if (author.getName().length() <= 4) {
            throw new EmptyDataException("Required Your Data must have 4 char up !!!");
        } else if (!(author.getGender().toLowerCase().equals("male") || author.getGender().toLowerCase().equals("female"))) {

            throw new EmptyDataException("Required Gender Must be : Male or Female !!!");

        }

        return AuthorMapper.INSTANCE.toDto(repository.update(id, author));
    }

    //delete
    @Override
    public void delete(Integer id) {

        AuthorDTO author = getById(id);

        repository.delete(author.getId());
    }
}
