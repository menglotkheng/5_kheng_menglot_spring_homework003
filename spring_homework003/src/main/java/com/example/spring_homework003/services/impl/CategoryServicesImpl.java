package com.example.spring_homework003.services.impl;


import com.example.spring_homework003.dto.CategoryDTO;
import com.example.spring_homework003.entity.request.CategoryRequest;
import com.example.spring_homework003.exception.EmptyDataException;
import com.example.spring_homework003.exception.NotFoundException;
import com.example.spring_homework003.mapper.CategoryMapper;
import com.example.spring_homework003.repository.CategoryRepository;
import com.example.spring_homework003.services.CategoryServices;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CategoryServicesImpl implements CategoryServices {


    private final CategoryRepository repository;


    //insert
    @Override
    public CategoryDTO insert(CategoryRequest category) {


        if (category.getName().isEmpty()) {

            throw new EmptyDataException("Required Data!!!");

        } else if (category.getName().length() <= 4) {
            throw new EmptyDataException("Required Your Data must have 4 char up !!!");
        }

        return CategoryMapper.INSTANCE.toDto(repository.insert(category));
    }

    //get by id
    @Override
    public CategoryDTO getById(Integer id) {

        if (repository.getById(id) == null) {
            throw new NotFoundException("Category ID : " + id + " Don't have in Database  😭🥲!!!");
        }

        return CategoryMapper.INSTANCE.toDto(repository.getById(id));
    }

    //get all
    @Override
    public List<CategoryDTO> gets() {

        if (repository.gets().isEmpty()) {
            throw new NotFoundException("Empty Data in Your Database 🥲😭!!!");
        }

        return repository.gets().stream().map(CategoryMapper.INSTANCE::toDto).toList();
    }


    //update
    @Override
    public CategoryDTO update(Integer id, CategoryRequest category) {

        getById(id);
        if (category.getName().isEmpty()) {

            throw new EmptyDataException("Required Data!!!");

        } else if (category.getName().length() <= 4) {
            throw new EmptyDataException("Required Your Data must have 4 char up !!!");
        }

        return CategoryMapper.INSTANCE.toDto(repository.update(id, category));
    }

    //delete
    @Override
    public void delete(Integer id) {

        CategoryDTO category = getById(id);

        repository.delete(category.getId());
    }
}
