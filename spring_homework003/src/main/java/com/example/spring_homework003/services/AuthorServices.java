package com.example.spring_homework003.services;

import com.example.spring_homework003.dto.AuthorDTO;
import com.example.spring_homework003.entity.request.AuthorRequest;

import java.util.List;

public interface AuthorServices {

    AuthorDTO insert(AuthorRequest author);

    AuthorDTO getById(Integer id);

    List<AuthorDTO> gets();

    AuthorDTO update(Integer id, AuthorRequest author);

    void delete(Integer id);

}
