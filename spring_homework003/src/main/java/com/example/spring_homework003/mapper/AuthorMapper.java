package com.example.spring_homework003.mapper;

import com.example.spring_homework003.dto.AuthorDTO;
import com.example.spring_homework003.entity.Author;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AuthorMapper {

    AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);


    AuthorDTO toDto(Author entity);

    Author toEntity(AuthorDTO dto);

}
