package com.example.spring_homework003.mapper;

import com.example.spring_homework003.dto.BookDTO;
import com.example.spring_homework003.entity.Book;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BookMapper {

    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    BookDTO toDto(Book entity);

    Book toEntity(BookDTO dto);

}
