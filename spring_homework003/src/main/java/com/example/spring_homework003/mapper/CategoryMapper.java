package com.example.spring_homework003.mapper;

import com.example.spring_homework003.dto.CategoryDTO;
import com.example.spring_homework003.entity.Category;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CategoryMapper {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    CategoryDTO toDto(Category entity);

    Category toEntity(CategoryDTO dto);

}
