package com.example.spring_homework003.Respone;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response<T> {

    private String message;
    private LocalDateTime date;

    private Boolean status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;

}
