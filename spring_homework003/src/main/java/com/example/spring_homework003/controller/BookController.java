package com.example.spring_homework003.controller;

import com.example.spring_homework003.Respone.Response;
import com.example.spring_homework003.dto.AuthorDTO;
import com.example.spring_homework003.dto.BookDTO;
import com.example.spring_homework003.entity.Book;
import com.example.spring_homework003.entity.request.BookRequest;
import com.example.spring_homework003.services.BookServices;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/book")
@AllArgsConstructor
public class BookController {

    private final BookServices services;


    //get all book
    @GetMapping(path = "/get")
    public ResponseEntity<?> get() {

        Response<List<Book>> response = Response.<List<Book>>builder()
                .message("Book Has Been get successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.gets())
                .build();
        return ResponseEntity.ok().body(response);
    }

    //get By id
    @GetMapping(path = "/get/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Integer id) {

        Response<Book> response = Response.<Book>builder()
                .message("Book Has Been get successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.getById(id))
                .build();
        return ResponseEntity.ok().body(response);
    }

    //delete
    @DeleteMapping(path = "/delete/{id}")
    ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {

        Response<AuthorDTO> response = Response.<AuthorDTO>builder()
                .message("Book ID: " + id + " Has Been delete successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(null)
                .build();
        services.delete(id);
        return ResponseEntity.ok().body(response);
    }

    //insert
    @PostMapping("/insert")
    public ResponseEntity<?> insert(@RequestBody BookRequest book) {
        Integer bookId = services.insert(book);
        Book books = services.getById(bookId);

        Response<Book> response = Response.<Book>builder()
                .message("Book Has Been insert successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(books)
                .build();
        return ResponseEntity.ok().body(response);
    }

    //update
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Integer id, @RequestBody BookRequest book) {
        Integer bookId = services.update(id, book);
        Book books = services.getById(bookId);

        Response<Book> response = Response.<Book>builder()
                .message("Book Has Been Update successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(books)
                .build();
        return ResponseEntity.ok().body(response);
    }

}
