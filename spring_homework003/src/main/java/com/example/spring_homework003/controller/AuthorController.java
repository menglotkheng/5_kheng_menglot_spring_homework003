package com.example.spring_homework003.controller;

import com.example.spring_homework003.Respone.Response;
import com.example.spring_homework003.dto.AuthorDTO;
import com.example.spring_homework003.entity.request.AuthorRequest;
import com.example.spring_homework003.services.AuthorServices;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/author")
@AllArgsConstructor
public class AuthorController {

    private final AuthorServices services;

    //insert
    @PostMapping(path = "/insert")
    public ResponseEntity<?> insert(@RequestBody AuthorRequest author) {

        Response<AuthorDTO> response = Response.<AuthorDTO>builder()
                .message("Author Has Been insert successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.insert(author))
                .build();

        return ResponseEntity.ok().body(response);
    }

    //get by id
    @GetMapping(path = "/get/{id}")
    ResponseEntity<?> getById(@PathVariable(value = "id") Integer id) {

        Response<AuthorDTO> response = Response.<AuthorDTO>builder()
                .message("Author Has Been get successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.getById(id))
                .build();

        return ResponseEntity.ok().body(response);
    }

    //delete
    @DeleteMapping(path = "/delete/{id}")
    ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {

        Response<AuthorDTO> response = Response.<AuthorDTO>builder()
                .message("Author ID: " + id + " Has Been delete successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(null)
                .build();
        services.delete(id);
        return ResponseEntity.ok().body(response);
    }

    //update
    @PutMapping(path = "/update/{id}")
    ResponseEntity<?> update(@PathVariable(value = "id") Integer id, @RequestBody AuthorRequest author) {

        Response<AuthorDTO> response = Response.<AuthorDTO>builder()
                .message("Author ID: " + id + " Has Been update successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.update(id, author))
                .build();
        return ResponseEntity.ok().body(response);
    }

    //get all
    @GetMapping(path = "/get")
    public ResponseEntity<?> get() {

        Response<List<AuthorDTO>> response = Response.<List<AuthorDTO>>builder()
                .message("Author Has Been get successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.gets())
                .build();

        return ResponseEntity.ok().body(response);
    }
}
