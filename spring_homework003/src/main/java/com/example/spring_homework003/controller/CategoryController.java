package com.example.spring_homework003.controller;

import com.example.spring_homework003.Respone.Response;
import com.example.spring_homework003.dto.CategoryDTO;
import com.example.spring_homework003.entity.request.CategoryRequest;
import com.example.spring_homework003.services.CategoryServices;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/category")
@AllArgsConstructor
public class CategoryController {

    private final CategoryServices services;

    //insert
    @PostMapping(path = "/insert")
    public ResponseEntity<?> insert(@RequestBody CategoryRequest category) {

        Response<CategoryDTO> response = Response.<CategoryDTO>builder()
                .message("Category Has Been insert successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.insert(category))
                .build();

        return ResponseEntity.ok().body(response);
    }

    //get by id
    @GetMapping(path = "/get/{id}")
    ResponseEntity<?> getById(@PathVariable(value = "id") Integer id) {

        Response<CategoryDTO> response = Response.<CategoryDTO>builder()
                .message("Category Has Been get successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.getById(id))
                .build();

        return ResponseEntity.ok().body(response);
    }

    //delete
    @DeleteMapping(path = "/delete/{id}")
    ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {

        Response<CategoryDTO> response = Response.<CategoryDTO>builder()
                .message("Category ID: " + id + " Has Been delete successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(null)
                .build();
        services.delete(id);
        return ResponseEntity.ok().body(response);
    }

    //update
    @PutMapping(path = "/update/{id}")
    ResponseEntity<?> update(@PathVariable(value = "id") Integer id, @RequestBody CategoryRequest category) {

        Response<CategoryDTO> response = Response.<CategoryDTO>builder()
                .message("Category ID: " + id + " Has Been update successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.update(id, category))
                .build();
        return ResponseEntity.ok().body(response);
    }

    //get all
    @GetMapping(path = "/get")
    public ResponseEntity<?> get() {

        Response<List<CategoryDTO>> response = Response.<List<CategoryDTO>>builder()
                .message("Author Has Been get successful")
                .date(LocalDateTime.now())
                .status(true)
                .payload(services.gets())
                .build();

        return ResponseEntity.ok().body(response);
    }

}
