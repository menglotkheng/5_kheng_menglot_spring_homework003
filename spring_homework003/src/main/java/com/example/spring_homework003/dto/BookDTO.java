package com.example.spring_homework003.dto;

import com.example.spring_homework003.entity.Author;
import com.example.spring_homework003.entity.Book;
import com.example.spring_homework003.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookDTO {

    private Integer id;
    private String title;

    private LocalDateTime date;

    private Author author_id;

    private List<Category> categories;

}
