package com.example.spring_homework003.entity.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BookRequest {

    private String title;

    private LocalDateTime date;

    private Integer authorId;

    private List<Integer> category;

}
