package com.example.spring_homework003.repository;

import com.example.spring_homework003.entity.Category;
import com.example.spring_homework003.entity.request.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {

    // get all of author
    @Select("SELECT * FROM categories;")
    @Results(
            id = "category", value = {
            @Result(property = "id", column = "category_id"),
            @Result(property = "name", column = "category_name"),

    }
    )
    List<Category> gets();


    //insert
    @Select("INSERT into categories(category_name)\n" +
            "VALUES (#{c.name}) RETURNING *")
    @ResultMap("category")
    Category insert(@Param("c") CategoryRequest category);


    //get by id
    @Select("SELECT * FROM categories WHERE category_id= #{id}")
    @ResultMap("category")
    Category getById(Integer id);


    //update
    @Select("UPDATE categories\n" +
            "SET category_name = #{c.name}\n" +
            "WHERE category_id=#{id} RETURNING *;")
    @ResultMap("category")
    Category update(Integer id, @Param("c") CategoryRequest category);

    //delete
    @Delete("DELETE FROM categories WHERE category_id=#{id}")
    void delete(Integer id);

}
