package com.example.spring_homework003.repository;

import com.example.spring_homework003.entity.Author;
import com.example.spring_homework003.entity.request.AuthorRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepository {


    // get all of author
    @Select("SELECT * FROM author;")
    @Results(
            id = "author", value = {
            @Result(property = "id", column = "author_id"),
            @Result(property = "name", column = "author_name"),
            @Result(property = "gender", column = "gender")

    }
    )
    List<Author> gets();


    //insert
    @Select("INSERT into author(author_name,gender)\n" +
            "VALUES (#{a.name},#{a.gender}) RETURNING *")
    @ResultMap("author")
    Author insert(@Param("a") AuthorRequest author);


    //get by id
    @Select("SELECT * FROM author WHERE author_id= #{id}")
    @ResultMap("author")
    Author getById(Integer id);


    //update
    @Select("UPDATE author\n" +
            "SET author_name = #{a.name}, gender = #{a.gender}\n" +
            "WHERE author_id=#{id} RETURNING *;")
    @ResultMap("author")
    Author update(Integer id, @Param("a") AuthorRequest author);

    //delete
    @Delete("DELETE FROM author WHERE author_id=#{id}")
    void delete(Integer id);


}
