package com.example.spring_homework003.repository;

import com.example.spring_homework003.entity.Book;
import com.example.spring_homework003.entity.Category;
import com.example.spring_homework003.entity.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {

    //get all product
    @Select("SELECT * FROM book_details INNER JOIN categories c on c.category_id = book_details.category_id WHERE book_id =#{id}")
    @Result(property = "id", column = "category_id")
    @Result(property = "name", column = "category_name")
    List<Category> getAllCategory(Integer id);


    //get all invoice
    @Select("select * from books")
    @Results(
            id = "book",
            value = {
                    @Result(property = "id", column = "book_id"),
                    @Result(property = "title", column = "title"),
                    @Result(property = "date", column = "published_date"),
                    @Result(property = "author_id", column = "author_id", one = @One(select = "com.example.spring_homework003.repository.AuthorRepository.getById")),
                    @Result(property = "category", column = "book_id", many = @Many(select = "getAllCategory")),
            }
    )
    List<Book> gets();


    //get By id
    @Select("select * from books where book_id=#{id}")
    @ResultMap("book")
    Book getById(Integer id);

    //delete
    @Delete("delete from books where book_id=#{id}")
    void delete(Integer id);

    //insert book
    @Select("insert into books(title,published_date, author_id) values(#{book.title}, #{book.date}, #{book.authorId})" +
            "returning book_id")
    Integer insertBook(@Param("book") BookRequest book);


    //insert book detail
    @Insert("insert into book_details(book_id,category_id) values(#{bookId},#{categoryId})")
    void insertBookDetail(Integer bookId, Integer categoryId);

    //update book
    @Select("""
            update books set title=#{book.title} ,author_id=#{book.authorId} where book_id=#{id}
            returning book_id
            """)
    Integer update(Integer id, @Param("book") BookRequest book);


    //delete book details
    @Delete("""
            delete from book_details where book_id=#{id}
            """)
    void deleteBookDetails(Integer id);
}
