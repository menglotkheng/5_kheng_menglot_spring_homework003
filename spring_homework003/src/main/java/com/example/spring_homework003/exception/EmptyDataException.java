package com.example.spring_homework003.exception;

public class EmptyDataException extends RuntimeException {

    public EmptyDataException(String message) {
        super(message);
    }
}
