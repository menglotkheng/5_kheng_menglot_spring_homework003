package com.example.spring_homework003.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;

@ControllerAdvice
public class GlobalException {

    @ExceptionHandler(EmptyDataException.class)
    ProblemDetail emptyHandler(EmptyDataException emptyException) {

        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(

                HttpStatus.BAD_REQUEST,

                emptyException.getMessage()

        );

        problemDetail.setTitle("Bad Request Data 😑😭!!!");
        problemDetail.setType(URI.create("localhost:8000/error/bad"));

        return problemDetail;
    }

    @ExceptionHandler(NotFoundException.class)
    ProblemDetail notFoundHandler(NotFoundException notFoundException) {

        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(

                HttpStatus.NOT_FOUND,

                notFoundException.getMessage()

        );

        problemDetail.setTitle("Not Found Data !!!");
        problemDetail.setType(URI.create("localhost:8000/error/not/found"));

        return problemDetail;
    }

}
