Database Creation : 


create database Book;

create table author(author_id serial primary key
                   ,author_name varchar(150) not null ,
                   gender varchar(10));

create table books(book_id serial primary key ,
title varchar(150),
published_date timestamp,author_id int,
constraint author_fk foreign key (author_id) references author(author_id)
)

create table categories(category_id serial primary key ,
category_name varchar(150) not null
)

create table book_details(
    id serial primary key ,
    book_id int,
    category_id int,
    constraint book_fk foreign key (book_id) references
                         books(book_id) on  delete  cascade
                         on update  cascade ,
    constraint category_fk foreign key (category_id) references categories(category_id)
)
